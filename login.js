var loginButton = document.getElementById('loginBtn');
var usernameInput = document.getElementById('usernameInput');
var passwordInput = document.getElementById('passwordInput');
loginButton.disabled = true;

usernameInput.addEventListener('input', enableButton);
passwordInput.addEventListener('input', enableButton);

function enableButton() {
  if (usernameInput.value.length !== 0 && passwordInput.value.length !== 0) {
    loginButton.disabled = false;
  } else {
    loginButton.disabled = true;
  }
};

loginButton.addEventListener('click', function() {
    var loginUsername = usernameInput.value;
    var loginPassword = passwordInput.value;
    if (window.localStorage.getItem('Users')) {
        var allStoredUsers = JSON.parse(localStorage.getItem('Users'));
        var matchedUser = allStoredUsers.filter(user => {
            return loginUsername === user.username && loginPassword === user.password;
        })
        if (matchedUser.length) {
            window.location.href = 'index.html';
        } else {
          var loginDiv = document.getElementById('loginDiv');
          var createUser = document.createElement('p');
          createUser.classList.add('createUser');
          createUser.innerHTML = 'Account does not exist. Please create your account.';
          loginDiv.appendChild(createUser);
            // console.log('Account does not exist. Please create your account.')
        }
      }
    //  else {
    //     console.log('Wrong credentials') // Don't say "Not a registered user"
    // }
})
