var addNewRestaurantBtn = document.getElementById('add-restaurant-btn')
var restaurantInput = document.getElementById('restaurant-input')
var listOfAddedRestaurants = document.getElementById('list');
addNewRestaurantBtn.disabled = true;

restaurantInput.addEventListener('input', function() {
  if (restaurantInput.value.length !== 0) {
    addNewRestaurantBtn.disabled = false;
  } else {
    addNewRestaurantBtn.disabled = true;
  }
});

addNewRestaurantBtn.addEventListener('click', function() {
  var newRestaurantItem = document.createElement('li');
  var openNewRestaurantBtn = document.createElement('button');
  openNewRestaurantBtn.innerHTML = '+';
  openNewRestaurantBtn.classList.add('openNewRestaurantBtn');
  newRestaurantItem.innerHTML = restaurantInput.value;
  listOfAddedRestaurants.appendChild(newRestaurantItem);
  listOfAddedRestaurants.appendChild(openNewRestaurantBtn);
  restaurantInput.value = "";

  openNewRestaurantBtn.addEventListener('click', function() {
    var container = document.getElementById('main-div');
    var parentDiv = document.getElementById('new-restaurants-div');
    var divForEachNewRestaurant = document.createElement('div');
    divForEachNewRestaurant.classList.add('divForEachNewRestaurant')
    parentDiv.appendChild(divForEachNewRestaurant);

    var headingOfNewDiv = document.createElement('h2');
    headingOfNewDiv.innerHTML = newRestaurantItem.textContent;
    headingOfNewDiv.classList.add('headingOfNewDiv');
    divForEachNewRestaurant.appendChild(headingOfNewDiv);

    var orderInput = document.createElement('input');
    orderInput.placeholder = 'Enter your order...';
    divForEachNewRestaurant.appendChild(orderInput);

    var addNewOrderBtn = document.createElement('button')
    addNewOrderBtn.innerHTML = '+';
    addNewOrderBtn.classList.add('addNewOrderBtn');
    divForEachNewRestaurant.appendChild(addNewOrderBtn);

    addNewOrderBtn.disabled = true;
    orderInput.addEventListener('input', function() {
      if (orderInput.value.length !== 0) {
        addNewOrderBtn.disabled = false;
      } else {
        addNewOrderBtn.disabled = true;
      }
    });

    addNewOrderBtn.addEventListener('click', function() {
      var orderListItem = document.createElement('li');
      orderListItem.classList.add('orderListItem');
      orderListItem.innerHTML = orderInput.value;
      divForEachNewRestaurant.appendChild(orderListItem);
      orderInput.value = "";
      addNewOrderBtn.disabled = true;
    });
    openNewRestaurantBtn.disabled = true;


  })
  addNewRestaurantBtn.disabled = true;
})
