var signUpButton = document.getElementById('signUpBtn');
var usernameInput = document.getElementById('usernameInput');
var passwordInput = document.getElementById('passwordInput');
signUpButton.disabled = true;

usernameInput.addEventListener('input', enableButton);
passwordInput.addEventListener('input', enableButton);

function enableButton() {
  if (usernameInput.value.length !== 0 && passwordInput.value.length !== 0) {
    signUpButton.disabled = false;
  } else {
    signUpButton.disabled = true;
  }
};

signUpButton.addEventListener('click', function() {
  var storedUsers = window.localStorage.Users ? JSON.parse(localStorage.Users) : [];
  var userData = {
    username: usernameInput.value,
    password: passwordInput.value
  };
  storedUsers.push(userData);
  localStorage.setItem('Users', JSON.stringify(storedUsers));
  window.location.href = 'login.html';
  usernameInput.value = '';
  passwordInput.value = '';

})
